import vendors from '../shared/vendors';
import handlers from './modules/handlers';
import scroll from './modules/scroll';
import * as sliders from './modules/sliders';

$(() => {
    vendors();
    handlers();
    sliders.init();
    scroll();
});

$(window).on("load", () => {
    setTimeout(() => {
        $(".preloader").addClass("preloader--done");
    }, 1000);
});