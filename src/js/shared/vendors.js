import {MOBILE_BREAKPOINT, EFFECT_TYPE} from './globals';
import {playAnimation} from './utils';
import appear from '../vendors/appear';
import 'lazysizes';

let fullPageCreated = false;

function createFullPage() {
    if (!fullPageCreated) {
        $('#fullpage').fullpage({
            scrollingSpeed: 500,
            scrollBar: true,
            lockAnchors: true,
            fitToSection: false,
            normalScrollElements: '.popup',
            responsiveWidth: MOBILE_BREAKPOINT,
            anchors: [
                'home',
                'lalala',
                'stages',
                'circle',
                'blblbl',
                'team'
            ]
        });

        fullPageCreated = true;
    }
}

function destroyFullPage() {
    if (fullPageCreated) {
        $
            .fn
            .fullpage
            .destroy('all');
        fullPageCreated = false;
    }
}

export default function () {
    $('.header__link').click(event => {
        if ($(window).outerWidth() >= MOBILE_BREAKPOINT) {
            event.preventDefault();
            const anchor = $(event.target).data('menuanchor');
            $
                .fn
                .fullpage
                .moveTo(anchor);
        } else {
            if (event.target.hash !== "") {
                event.preventDefault();
                const hash = event.target.hash;
                $('html, body ').animate({
                    scrollTop: $(hash)
                        .offset()
                        .top
                }, 700, function () {
                    window.location.hash = hash;
                });
            }
        }
    });

    let fadeUpOnScroll = appear({
        elements() {
            return document.getElementsByClassName('animated');
        },
        appear: function appear(el) {
            playAnimation(el, EFFECT_TYPE.fadeUp);
        },
        bounds: 100,
        reappear: true
    });

    if (!($(window).outerWidth() < MOBILE_BREAKPOINT)) {
        createFullPage();
    } else {
        destroyFullPage();
        fadeUpOnScroll.destroy();
    }

    $(window).resize(() => {
        $(window).outerWidth() < MOBILE_BREAKPOINT
            ? destroyFullPage()
            : createFullPage();
    });
}