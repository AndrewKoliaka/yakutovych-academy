export const UKR = {
    imgCaptions: [
        {
            description: 'Сергій Якутович. Батько. З серії «Кияни».',
            copyright: '1986, офорт'
        }, {
            description: 'Георгій Якутович. Ася.',
            copyright: '1971, папір, олівець'
        }, {
            description: 'Сергій Якутович. Автопортрет.',
            copyright: '1990-ті, полотно, олія'
        }, {
            description: 'Сергій Якутович. Оля і я. З серії «Кияни».',
            copyright: '1986, офорт'
        }, {
            description: 'Сергій Якутович. Брат. З серії «Кияни».',
            copyright: '1986, офорт'
        }, {
            description: 'Антон Якутович. Автопортрет.',
            copyright: '1991, полотно, олія'
        }
    ],
    facts: [
        'У дев’ять років Сергій Якутович знявся у фільмі Андрія Тарковського «Іванове дитинство» в ролі маленького Івана.',
        'Коли у 1960-х роках митця Григорія Гавриленка звільнили з Київського художнього інституту за інакодумство, Георгій Якутович залишив свою посаду в КХІ у знак протесту.',
        'У відділі КГБ на Георгія Якутовича в 1959 році була заведена справа, де художник значився як «антисоціальний елемент».',
        'Перша персональна виставка Георгія Якутовича відбулася в останній рік його життя – у 2000-му.',
        'Один з прадідів Георгія Якутовича товаришував з Тарасом Шевченком, тоді як його бабця була машиністкою в секретаріаті Михайла Грушевського.',
        'У 2009 році Сергій Якутович розробив Великий Державний Герб України, який так і не був остаточно утверджений.',
        'Георгій Якутович пов’язав своє життя та творчість з Карпатами: мав там будиночок, їздив у гори кожного року, а на свої 60 років охрестився в Ільцях.',
        'На графічний факультет Георгій Якутович пішов, зокрема, через дальтонізм.',
        'Роботи Ольги Якутович користувалися особливою популярністю в Японії.',
        'До роботи в театрі Сергія Якутовича залучив Богдан Ступка.',
        'До 12 років Дмитро Якутович займався музикою, однак потрапив з батьками в автокатастрофу та втратив слух. Після цього хлопець звернувся до живопису.',
        'Декорації до фільму «Молитва за гетьмана Мазепу», створені Сергієм Якутовичем, були спалені під час зйомок.'
    ]
}

export const EN = {
    imgCaptions: [
        {
            description: 'Serhiy Yakutovych. Father. From People of Kyiv series.',
            copyright: '1986, etching'
        }, {
            description: 'Heorhiy Yakutovych. Asya.',
            copyright: '1971, pencil on paper'
        }, {
            description: 'Serhiy Yakutovych. Self-portrait.',
            copyright: '1990s, oil on canvas'
        }, {
            description: 'Serhiy Yakutovych. Olha and I. From People of Kyiv series.',
            copyright: '1986, etching'
        }, {
            description: 'Serhiy Yakutovych. Brother. From People of Kyiv series.',
            copyright: '1986, etching'
        }, {
            description: 'Anton Yakutovych. Self-portrait.',
            copyright: '1991, oil on canvas'
        }
    ],
    facts: [
        'When Serhiy Yakutovych was nine, he played a little Ivan in Andrey Tarkovsky’s film Ivan’s Childhood.',
        'When in 1960s the artist Hryhoriy Havrylenko was dismissed from the Kyiv Art Institute for dissidence, Heorhiy Yakutovych protested by quitting his job at the Institute.',
        'Since 1959, the KGB (Committee for State Security) had a file on Heorhiy Yakutovych where the painter was described as an “antisocial character”.',
        'The first personal exhibition of Heorhiy Yakutovych was held during the last year of his life – in 2000.',
        'One of Heorhiy Yakutovych’s great grandfathers was friends with Taras Shevchenko, while his grandmother was a typist at the Mykhailo Hrushevskyi’s Secretariat.',
        'In 2009, Serhiy Yakutovych designed the Great State Coat of Arms of Ukraine that was never finally approved.',
        'Heorhiy Yakutovych connected his life and work with the Carpathians: he had a house there, did some hiking every year and was baptized in Iltsi village when he was 60.',
        'One of the reasons why Heorhiy Yakutovych chose the graphic art faculty was his colour blindness.',
        'Olha Yakutovych’s works were very popular in Japan.',
        'It was Bohdan Stupka that involved Serhiy Yakutovych in theatre.',
        'Dmytro Yakutovych studied music until 12, but then he got into a car accident with his parents which resulted in the hearing loss. He started painting after that.',
        'The decorations to the film A Prayer for Hetman Mazepa created by Serhiy Yakutovych were burned during the shooting.'
    ]
}