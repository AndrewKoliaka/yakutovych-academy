'use strict';
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    cssnano = require('gulp-cssnano'),
    plumber = require('gulp-plumber'),
    spritesmith = require('gulp.spritesmith'),
    svgSprite = require('gulp-svg-sprite'),
    connect = require('gulp-connect'),
    pug = require('gulp-pug'),
    del = require('del'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    babelify = require('babelify'),
    buffer = require('vinyl-buffer'),
    rename = require('gulp-rename');

var path = {
    build: {
        html: {
            en: 'public/en/',
            ukr: 'public/ukr/'
        },
        js: 'public/js/',
        css: 'public/css/',
        img: 'public/img/',
        fonts: 'public/fonts/',
        svgIcons: 'public/'
    },
    prod: {
        html: {
            en: 'prod/en/',
            ukr: 'prod/ukr/'
        },
        js: 'prod/js/',
        css: 'prod/css/',
        img: 'prod/img/',
        fonts: 'prod/fonts/',
        php: 'prod/'
    },
    src: {
        pug: {
            en: ['src/views/en/index/index.pug'],
            ukr: ['src/views/ukr/index/index.pug', 'src/views/ukr/01/01.pug']
        },
        js: ['src/js/index/index.js'],
        style: ['src/sass/index/index.scss', 'src/sass/01/01.scss'],
        sprite_scss: 'src/sass/base/',
        img: 'src/img/**/*.*',
        sprite_img: 'src/icons/*.png',
        fonts: 'src/fonts/**/*.*',
        svgIcons: 'src/icons/*.svg',
        php: 'server/*.php',
        inlineStyle: 'src/sass/inline/index.scss'
    },
    watch: {
        pug: 'src/views/**/**/*.pug',
        js: 'src/js/**/*.js',
        style: 'src/sass/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        svgIcons: 'src/icons/*.svg'
    }
};

gulp.task('webserver', function () {
    connect.server({root: 'public/', livereload: true, port: 3000});
});

gulp.task('del', function (cb) {
    del(['./public', './prod']);
});

gulp.task('pug:build', function () {
    path
        .src
        .pug
        .ukr
        .forEach(file => {
            gulp
                .src(file)
                .pipe(pug())
                .pipe(gulp.dest(path.build.html.ukr))
                .pipe(connect.reload());
        });

    path
        .src
        .pug
        .en
        .forEach(file => {
            gulp
                .src(file)
                .pipe(pug())
                .pipe(gulp.dest(path.build.html.en))
                .pipe(connect.reload());
        });
});

gulp.task('pug:prod', function () {
    path
        .src
        .pug
        .ukr
        .forEach(file => {
            gulp
                .src(file)
                .pipe(pug())
                .pipe(rename('index.php'))
                .pipe(gulp.dest(path.prod.html.ukr))
                .pipe(connect.reload());
        });

    path
        .src
        .pug
        .en
        .forEach(file => {
            gulp
                .src(file)
                .pipe(pug())
                .pipe(rename('index.php'))
                .pipe(gulp.dest(path.prod.html.en))
                .pipe(connect.reload());
        });
});

gulp.task('js:build', function () {

    path
        .src
        .js
        .forEach(filePath => {
            let fileName = filePath.substring(filePath.lastIndexOf('/') + 1);

            browserify(filePath)
                .transform("babelify", {presets: ["es2015"]})
                .bundle()
                .pipe(source(fileName))
                .pipe(gulp.dest(path.build.js))
                .pipe(connect.reload());
        });
});

gulp.task('js:prod', function () {

    path
        .src
        .js
        .forEach(filePath => {
            let fileName = filePath.substring(filePath.lastIndexOf('/') + 1);

            browserify(filePath)
                .transform("babelify", {presets: ["es2015"]})
                .bundle()
                .pipe(source(fileName))
                .pipe(buffer())
                .pipe(sourcemaps.init())
                .pipe(uglify())
                .pipe(sourcemaps.write('.'))
                .pipe(gulp.dest(path.prod.js));
        });
});

gulp.task('style:build', function () {
    path
        .src
        .style
        .forEach(filePath => {
            gulp
                .src(filePath)
                .pipe(plumber())
                .pipe(sourcemaps.init())
                .pipe(sass({sourceMap: false, errLogToConsole: true, outputStyle: 'expanded'}))
                .pipe(prefixer({
                    browsers: [
                        'last 3 versions', 'IE >= 10'
                    ],
                    cascade: false
                }))
                .pipe(sourcemaps.write())
                .pipe(gulp.dest(path.build.css))
                .pipe(connect.reload());
        });
});

gulp.task('style:prod', function () {
    path
        .src
        .style
        .forEach(filePath => {
            gulp
                .src(filePath)
                .pipe(plumber())
                .pipe(sass({sourceMap: false, errLogToConsole: true}))
                .pipe(prefixer({
                    browsers: [
                        'last 3 versions', 'IE >= 10'
                    ],
                    cascade: false
                }))
                .pipe(cssnano())
                .pipe(gulp.dest(path.prod.css));
        });
});

gulp.task('image:build', function () {
    gulp
        .src(path.src.img)
        .pipe(gulp.dest(path.build.img))
        .pipe(connect.reload());
});

gulp.task('image:prod', function () {
    gulp
        .src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            optimizationLevel: 10,
            svgoPlugins: [
                {
                    removeViewBox: false
                }
            ],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.prod.img));
});

gulp.task('fonts:build', function () {
    gulp
        .src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('fonts:prod', function () {
    gulp
        .src(path.src.fonts)
        .pipe(gulp.dest(path.prod.fonts))
});

gulp.task('sprite-create', function () {

    var spriteData = gulp
        .src(path.src.sprite_img)
        .pipe(spritesmith({imgName: '../img/spritesheet.png', cssName: '_sprite.scss', cssFormat: 'scss', padding: 5}));

    spriteData
        .img
        .pipe(gulp.dest(path.build.img));

    spriteData
        .css
        .pipe(gulp.dest(path.src.sprite_scss));

    return spriteData;
});

gulp.task('sprite-create:prod', function () {

    var spriteData = gulp
        .src(path.src.sprite_img)
        .pipe(spritesmith({imgName: '../img/spritesheet.png', cssName: '_sprite.scss', cssFormat: 'scss', padding: 5}));

    spriteData
        .img
        .pipe(gulp.dest(path.prod.img));

    spriteData
        .css
        .pipe(gulp.dest(path.src.sprite_scss));

    return spriteData;
});

gulp.task('svg:build', function () {
    return gulp
        .src(path.src.svgIcons)
        .pipe(svgSprite({
            mode: {
                symbol: {
                    dest: "./",
                    sprite: 'img/sprite'
                }
            },
            shape: {
                spacing: {
                    padding: 0
                }
            },
            variables: {
                mapname: "sprite"
            }
        }))
        .pipe(gulp.dest('./public'));
});

gulp.task('php', function () {
    gulp
        .src(path.src.php)
        .pipe(gulp.dest(path.prod.php));
});

gulp.task('watch', function () {
    watch([path.watch.pug], function (event, cb) {
        gulp.start('pug:build');
    });
    watch([path.watch.style], function (event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function (event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function (event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function (event, cb) {
        gulp.start('fonts:build');
    });
    watch([path.src.sprite_img], function (event, cb) {
        gulp.start('sprite-create');
    });
    watch([path.src.svgIcons], function (event, cb) {
        gulp.start('svg:build');
    });
});

gulp.task('build', [
    'pug:build',
    'js:build',
    'sprite-create',
    'svg:build',
    'style:build',
    'fonts:build',
    'image:build'
]);

gulp.task('prod', [
    'pug:prod',
    'js:prod',
    'sprite-create:prod',
    'svg:build',
    'style:prod',
    'fonts:prod',
    'image:prod',
    'php'
]);

gulp.task('default', ['build', 'webserver', 'watch']);