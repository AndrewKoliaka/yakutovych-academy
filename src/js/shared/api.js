const URL = "db.php";

export function addSubscriber(email, cb) {
    $.post(URL, {
        email
    }, cb);
}