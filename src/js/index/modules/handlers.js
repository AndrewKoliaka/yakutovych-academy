import * as circles from './circles';
import {EN, UKR} from './content';
import * as utils from '../../shared/utils';
import {addSubscriber} from '../../shared/api';
import {MOBILE_BREAKPOINT} from '../../shared/globals';

const LANG = {
    ukr: 'ukr',
    en: 'en'
}

export default function () {

    let homeInitialHeight = null;
    let selectedIndex = null;
    let personHeight = null;
    let familyHeight = null;
    let content = null;
    const circleInitialPadding = $('.circle').css('padding-bottom');
    const contentInitialHeight = $('.family__content').height();

    switch (sessionStorage.getItem('lang')) {
        case LANG.ukr:
            content = UKR;
            break;
        case LANG.en:
            content = EN;
            break;
        default:
            content = UKR;
    }

    circles.init();

    $(document).click(event => {
        let target = $(event.target);

        if (!target.hasClass('facts__link')) {
            $('.facts__link').removeClass('facts__link--active');
            $('.facts__fact').fadeOut();
        }

        if (!target.hasClass('team__info') && $('.team__info').is(':visible')) {
            $('.team__info').slideUp();
            $('.team__more-btn').css('opacity', '1');
        }

        if (!target.hasClass('team__input') && !target.hasClass('team__submit')) {
            $('.team__label').fadeIn();
            $('.team__error-message').fadeOut();
        }
    });

    $('.facts__link').click(event => {
        event.preventDefault();

        const activeModifier = 'facts__link--active';
        const facts = content.facts;
        let target = $(event.target);
        let factContainer = target.next();

        if (target.hasClass(activeModifier)) {
            factContainer.fadeOut(() => {
                factContainer.text(() => (facts[utils.getRandomUniqueInt(0, facts.length)]));
            });

            target.removeClass(activeModifier);
        } else {
            factContainer.text(() => (facts[utils.getRandomUniqueInt(0, facts.length)]));
        }

        setTimeout(() => {
            target.addClass(activeModifier);
        }, 200);

        factContainer.fadeIn();
    });

    $('.person__img').click(event => {
        const index = $(event.target)
            .parent()
            .parent()
            .parent()
            .data('index');

        const captions = content.imgCaptions;

        $('.popup__img')
            .attr('src', $(event.target).attr('src'))
            .attr('alt', captions[index].description);
        $('.popup__description').text(captions[index].description);
        $('.popup__copyright').text(captions[index].copyright);
        $('.popup').show();
        $(document.body).css('overflow', 'hidden');
    });

    $('.team__label').click(event => {
        event.stopPropagation();
        $(event.target).fadeOut();
        $('.team__input').focus();
    });

    $('.team__info').slideUp();

    $('.team__info').click(event => {
        event.stopPropagation();
    });

    $('.team__more-btn').click(event => {
        event.preventDefault();
        event.stopPropagation();

        $(event.currentTarget).css('opacity', $('.team__info').is(':visible')
            ? 1
            : 0);

        $('.team__info').slideToggle();

    });

    $('.home__about-btn').click(event => {
        event.preventDefault();
        $('.about').addClass('about--active');
        let diff = $('.about__wrapper').outerHeight() - $('.home__content').outerHeight() - 100;

        if ($(window).outerWidth() < 700) {
            homeInitialHeight = $('.home').height();

            $('.home').animate({
                height: homeInitialHeight + diff
            }, 500);
        }
    });

    $('.about__back').click(event => {
        event.preventDefault();
        $('.about').removeClass('about--active');
        if ($(window).outerWidth() < 700) {
            $('.home').animate({
                height: homeInitialHeight
            }, 500);
        }
    });

    $('.circle__watch-btn').click(event => {
        event.preventDefault();
        circles.start();
        $('.family').addClass('family--active');
    });

    $('.family__back-btn').click(event => {
        event.preventDefault();
        $('.family').removeClass('family--active');
        $('.person').removeClass('person--active');
        $('.spinner__dot').removeClass('spinner__dot--active');
        circles.pause();
    });

    $('.family__item').click(event => {

        if ($(window).outerWidth() < MOBILE_BREAKPOINT) {

            personHeight = $(event.target)
                .find('.person')
                .outerHeight();

            let contentHeight = $('.family__content').outerHeight();

            $('.family__content').animate({
                height: personHeight + 'px'
            }, 500);
            $('.circle').animate({
                paddingBottom: (personHeight - contentHeight) + 'px'
            }, 500);
        }

        $(event.currentTarget)
            .find('.person')
            .addClass('person--active');

        selectedIndex = $(event.currentTarget).index();

        $('.spinner__dot').removeClass('spinner__dot--active');
        $($('.spinner__dot').get(selectedIndex)).addClass('spinner__dot--active');
    });

    $('.spinner__dot').click(event => {
        let index = $(event.target).data('index');

        if ($('.person').hasClass('person--active')) {
            if ($(window).outerWidth() < MOBILE_BREAKPOINT) {
                $('.circle').css({paddingBottom: circleInitialPadding});
                $('.family__content').css({height: contentInitialHeight});
            }
        }

        $('.person').removeClass('person--active');
        $($('.family__item').get(index)).click();
    });

    $('.person__back').click(event => {
        event.preventDefault();
        event.stopPropagation();

        $(event.target)
            .parent()
            .parent()
            .removeClass('person--active');

        if ($(window).outerWidth() < MOBILE_BREAKPOINT) {
            $('.circle').animate({
                paddingBottom: circleInitialPadding
            }, 500);

            $('.family__content').animate({
                height: contentInitialHeight
            }, 500);

            $('html, body').animate({
                scrollTop: $(".circle")
                    .offset()
                    .top
            }, 500);
        }

        $('.spinner__dot').removeClass('spinner__dot--active');
    });

    $('.popup__close').click(event => {
        event.preventDefault();
        $('.popup').hide();
        $(document.body).css('overflow', 'auto');
    });

    $('.header__select').change(event => {
        const val = event.target.value;

        if (val === LANG.en) {
            $('#link-en')[0].click();
        } else if (val === LANG.ukr) {
            $('#link-ukr')[0].click();
        }
    });

    $('.team__submit').click(event => {
        event.preventDefault();
        event.stopPropagation();

        if ($('.team__label').is(':visible')) {
            $('.team__label').fadeOut();
            return;
        }

        let value = $('.team__input').val();
        let success = () => {
            $('.team__input').val("");
            $('.team__label').fadeIn();
            $('.alert').fadeIn('fast');
            $('.alert__button').focus();
        }

        if (utils.validateEmail(value)) {
            $('.team__error-message').fadeOut();
            addSubscriber(value, success);
        } else {
            $('.team__error-message').fadeIn();
            $('.team__input').val("");
        }
    });

    $('.alert__wrapper').click(() => {
        $('.alert').fadeOut('fast');
    });

    $('.alert__container').click(event => {
        event.stopPropagation();
    });

    $('.alert__button').click(() => {
        $('.alert').fadeOut('fast');
    });
}