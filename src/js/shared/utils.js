import {MOBILE_BREAKPOINT} from './globals';

/**
 * Play animation for element
 *
 * @param {Object|String} Selector
 */
export function playAnimation(element, effectName) {
    if ($(window).outerWidth() >= MOBILE_BREAKPOINT) {
        let el = $(element);

        if (!el.data('isPlayed')) {
            el.removeClass(effectName);
            setTimeout(() => {
                el.addClass(effectName);
            }, 10);

            el.data('isPlayed', true);
        }
    }
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 *
 * @param {Number} min - Min edge of range
 * @param {Number} max - Max edge of range
 * @returns {Number}
 */
export function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Calculation based on previous returned values
 *
 * @param {Number} min - Min edge of range
 * @param {Number} max - Max edge of range
 * @returns {Number}
 */
export let getRandomUniqueInt = (() => {
    let used = [];

    return (min, max) => {
        let number = null;
        let maxLength = min === 0 ?
            max :
            max + 1;

        if (used.length === maxLength) {
            used = [];
        }

        do {
            number = getRandomInt(min, max);

            if (used.indexOf(number) === -1) {
                used.push(number);
                return number;
            }
        } while (true);
    }
})();

/**
 * Check is email address valid
 *
 * @param {String} email
 * @returns {Boolean} - result of validation
 */
export function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}

/**
 * Try to retrun cookie value by key
 *
 * @param {String} name - key of cookie
 * @returns {String|Null} - value of cookie or null if one doesn't exist
 */
export function getCookie(name) {
    let value = "; " + document.cookie;
    let parts = value.split("; " + name + "=");
    return parts.length == 2 ?
        parts
        .pop()
        .split(";")
        .shift() :
        null;
}