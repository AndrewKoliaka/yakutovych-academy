import Swiper from 'swiper';
import {MOBILE_BREAKPOINT} from '../../shared/globals';

let leftSlider = null,
    rightSlider = null,
    stagesSlider = null;

let interval = null;

function initHome() {
    const WINDOW_WIDTH = $(window).outerWidth();
    const SPEED = 5000;

    const config = {
        direction: 'vertical',
        speed: 1000,
        loop: true,
        observer: true,
        allowTouchMove: false,
        autoplay: false,
        breakpoints: {
            1025: {
                direction: 'horizontal',
                observer: true
            }
        }
    };

    leftSlider = new Swiper('.home__left-slider', config);
    rightSlider = new Swiper('.home__right-slider', config);

    if (WINDOW_WIDTH < MOBILE_BREAKPOINT) {
        leftSlider.removeSlide([3, 4]);
        rightSlider.removeSlide([3, 4]);
    }

    if (interval) {
        clearInterval(interval);
    }

    interval = setInterval(() => {
        if (WINDOW_WIDTH > MOBILE_BREAKPOINT) {
            leftSlider.slideNext();
            rightSlider.slidePrev();
        } else {
            leftSlider.slidePrev();
            rightSlider.slideNext();
        }
    }, SPEED);
}

function initStages() {
    const config = {
        slidesPerView: 3,
        centeredSlides: true,
        spaceBetween: 50,
        allowTouchMove: true,
        slideActiveClass: 'stages__slide--active',
        disabledClass: 'stages__control--disabled',
        autoHeight: true,
        navigation: {
            nextEl: '.stages__control--next',
            prevEl: '.stages__control--prev'
        },
        breakpoints: {
            1200: {
                slidesPerView: 3,
                centeredSlides: true,
                allowTouchMove: false,
                spaceBetween: 20
            },
            1025: {
                slidesPerView: 1,
                centeredSlides: false,
                allowTouchMove: true
            }
        }
    };

    stagesSlider = new Swiper('.stages__slider', config);
}

export function init() {
    initHome();
    initStages();

    $(window).on('resize', () => {
        leftSlider.destroy();
        rightSlider.destroy();
        initHome();
        stagesSlider.update();
    });
}