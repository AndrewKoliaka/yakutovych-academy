import Rellax from 'rellax';
import {MOBILE_BREAKPOINT} from '../../shared/globals';

let logoRellax,
    rabbitRellax;

function initObjects() {
    const logoSelector = '.stages__img-container--logo';
    const rabbitSelector = '.stages__img-container--rabbit';

    if (logoRellax) {
        logoRellax.destroy();
    }

    if (rabbitRellax) {
        rabbitRellax.destroy();
    }

    if ($(window).outerWidth() < MOBILE_BREAKPOINT) {
        logoRellax = new Rellax(logoSelector, {speed: -1});
        rabbitRellax = new Rellax(rabbitSelector, {speed: 2});
    } else {
        logoRellax = new Rellax(logoSelector, {speed: -1.5});
        rabbitRellax = new Rellax(rabbitSelector, {speed: 3});
    }
}

export default function () {
    initObjects();

    $(window).on('resize', () => {
        initObjects();
    });
};