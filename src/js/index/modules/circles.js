import anime from 'animejs';
import {
    MOBILE_BREAKPOINT
} from '../../shared/globals';

let animations = [];

let SPINNER = {
    lg: '#spinner__svg--lg',
    md: '#spinner__svg--md'
}

let current = null;

function initAnimations() {

    let motionPath1,
        motionPath2,
        motionPath3,
        motionPath4,
        motionPath5,
        motionPath6;

    let path1,
        path2,
        path3;

    var width = $(window).outerWidth();
    const DURATION = 11000;

    if (width < MOBILE_BREAKPOINT) {
        return;
    } else if (width < 1300 && current != SPINNER.md) {
        path1 = anime.path('#spinner__svg--md .circle1');
        path2 = anime.path('#spinner__svg--md .circle2');
        path3 = anime.path('#spinner__svg--md .circle3');
        current = SPINNER.md;
    } else if (current != SPINNER.lg) {
        path1 = anime.path('#spinner__svg--lg .circle1');
        path2 = anime.path('#spinner__svg--lg .circle2');
        path3 = anime.path('#spinner__svg--lg .circle3');
        current = SPINNER.lg;
    }

    animations = [];


    anime({
        targets: '.spinner__dot--1',
        translateY: path3('y'),
        translateX: path3('x'),
        easing: 'linear',
        duration: DURATION / 10,
        offset: 1000,
        complete() {
            motionPath1 = anime({
                targets: '.spinner__dot--1',
                translateY: path3('y'),
                translateX: path3('x'),
                easing: 'linear',
                duration: DURATION,
                loop: true
            });

            animations.push(motionPath1);
        }
    });



    anime({
        targets: '.spinner__dot--2',
        translateY: path3('y'),
        translateX: path3('x'),
        easing: 'linear',
        duration: DURATION / 10,
        complete() {
            motionPath2 = anime({
                targets: '.spinner__dot--2',
                translateY: path3('y'),
                translateX: path3('x'),
                easing: 'linear',
                duration: DURATION,
                loop: true
            });
            animations.push(motionPath2);
        }


    });

    anime({
        targets: '.spinner__dot--3',
        translateY: path2('y'),
        translateX: path2('x'),
        easing: 'linear',
        duration: 50,
        direction: 'reverse',
        offset: 3000,
        complete() {
            motionPath3 = anime({
                targets: '.spinner__dot--3',
                translateY: path2('y'),
                translateX: path2('x'),
                easing: 'linear',
                duration: DURATION,
                direction: 'reverse',
                loop: true
            });

            animations.push(motionPath3);
        }
    });

    anime({
        targets: '.spinner__dot--4',
        translateY: path2('y'),
        translateX: path2('x'),
        easing: 'linear',
        duration: 50,
        direction: 'reverse',
        offset: 3500,
        complete() {
            motionPath4 = anime({
                targets: '.spinner__dot--4',
                translateY: path2('y'),
                translateX: path2('x'),
                easing: 'linear',
                duration: DURATION,
                direction: 'reverse',
                loop: true,
            });

            animations.push(motionPath4);
        }
    });

    anime({
        targets: '.spinner__dot--5',
        translateY: path2('y'),
        translateX: path2('x'),
        easing: 'linear',
        duration: DURATION / 10,
        direction: 'reverse',
        offset: 600,
        complete() {
            motionPath5 = anime({
                targets: '.spinner__dot--5',
                translateY: path2('y'),
                translateX: path2('x'),
                easing: 'linear',
                duration: DURATION,
                direction: 'reverse',
                loop: true
            });

            animations.push(motionPath5);
        }
    });

    motionPath6 = anime({
        targets: '.spinner__dot--6',
        translateY: path1('y'),
        translateX: path1('x'),
        easing: 'linear',
        duration: DURATION,
        direction: 'reverse',
        loop: true
    });

    animations.push(motionPath6);
}

export function init() {
    initAnimations();

    $(window).blur(() => {
        pause();
    });

    $(window).focus(() => {
        start();
    });
}

export function start() {
    animations.forEach(el => el.play());
}

export function pause() {
    animations.forEach(el => el.pause());
}